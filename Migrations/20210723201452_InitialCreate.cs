﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace WebAppPeliculas.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Clientes",
                columns: table => new
                {
                    ClienteID = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Ci = table.Column<int>(type: "integer", nullable: false),
                    nombre = table.Column<string>(type: "text", nullable: true),
                    apellidos = table.Column<string>(type: "text", nullable: true),
                    direccion = table.Column<string>(type: "text", nullable: true),
                    email = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Clientes", x => x.ClienteID);
                });

            migrationBuilder.CreateTable(
                name: "Peliculas",
                columns: table => new
                {
                    PeliculaID = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Titulo = table.Column<string>(type: "text", nullable: true),
                    Anio = table.Column<int>(type: "integer", nullable: false),
                    Critica = table.Column<string>(type: "text", nullable: true),
                    Caratula = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Peliculas", x => x.PeliculaID);
                });

            migrationBuilder.CreateTable(
                name: "Copias",
                columns: table => new
                {
                    CopiaID = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Deteriorada = table.Column<bool>(type: "boolean", nullable: false),
                    formatos = table.Column<int>(type: "integer", nullable: false),
                    Precio_alquiler = table.Column<decimal>(type: "numeric", nullable: false),
                    PeliculaID = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Copias", x => x.CopiaID);
                    table.ForeignKey(
                        name: "FK_Copias_Peliculas_PeliculaID",
                        column: x => x.PeliculaID,
                        principalTable: "Peliculas",
                        principalColumn: "PeliculaID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Prestamos",
                columns: table => new
                {
                    PrestamoID = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Fecha_Prestamos = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Fecha_Tope = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    fecha_entrega = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    ClienteID = table.Column<int>(type: "integer", nullable: false),
                    CopiaID = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Prestamos", x => x.PrestamoID);
                    table.ForeignKey(
                        name: "FK_Prestamos_Clientes_ClienteID",
                        column: x => x.ClienteID,
                        principalTable: "Clientes",
                        principalColumn: "ClienteID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Prestamos_Copias_CopiaID",
                        column: x => x.CopiaID,
                        principalTable: "Copias",
                        principalColumn: "CopiaID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Copias_PeliculaID",
                table: "Copias",
                column: "PeliculaID");

            migrationBuilder.CreateIndex(
                name: "IX_Prestamos_ClienteID",
                table: "Prestamos",
                column: "ClienteID");

            migrationBuilder.CreateIndex(
                name: "IX_Prestamos_CopiaID",
                table: "Prestamos",
                column: "CopiaID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Prestamos");

            migrationBuilder.DropTable(
                name: "Clientes");

            migrationBuilder.DropTable(
                name: "Copias");

            migrationBuilder.DropTable(
                name: "Peliculas");
        }
    }
}
