﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAppPeliculas.Models;
namespace WebAppPeliculas.Data
{
    public class DbInitializer
    {
        public static void Initialize(PeliculaContext context)
        {
            context.Database.EnsureCreated();

            // Look for any students.
            if (context.Peliculas.Any())
            {
                return;   // DB has been seeded
            }

            
        }
        }
}
