﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using WebAppPeliculas.Models;
namespace WebAppPeliculas.Data
{
    public class PeliculaContext : IdentityDbContext
    {
        public PeliculaContext(DbContextOptions<PeliculaContext> options) : base(options)
        {
        }

        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Copia> Copias { get; set; }
        public DbSet<Prestamo> Prestamos { get; set; }
        public DbSet<Pelicula> Peliculas { get; set; }
    }
}
