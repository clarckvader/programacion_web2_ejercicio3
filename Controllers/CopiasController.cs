﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebAppPeliculas.Data;
using WebAppPeliculas.Models;

namespace WebAppPeliculas.Controllers
{
    public class CopiasController : Controller
    {
        private readonly PeliculaContext _context;

        public CopiasController(PeliculaContext context)
        {
            _context = context;
        }

        // GET: Copias
        public async Task<IActionResult> Index()
        {
            var peliculaContext = _context.Copias.Include(c => c.Pelicula);
            return View(await peliculaContext.ToListAsync());
        }

        // GET: Copias/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var copia = await _context.Copias
                .Include(c => c.Pelicula)
                .FirstOrDefaultAsync(m => m.CopiaID == id);
            if (copia == null)
            {
                return NotFound();
            }

            return View(copia);
        }

        // GET: Copias/Create
        public IActionResult Create()
        {
            ViewData["PeliculaID"] = new SelectList(_context.Peliculas, "PeliculaID", "PeliculaID");
            return View();
        }

        // POST: Copias/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CopiaID,Deteriorada,formatos,Precio_alquiler,PeliculaID")] Copia copia)
        {
            if (ModelState.IsValid)
            {
                _context.Add(copia);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["PeliculaID"] = new SelectList(_context.Peliculas, "PeliculaID", "PeliculaID", copia.PeliculaID);
            return View(copia);
        }

        // GET: Copias/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var copia = await _context.Copias.FindAsync(id);
            if (copia == null)
            {
                return NotFound();
            }
            ViewData["PeliculaID"] = new SelectList(_context.Peliculas, "PeliculaID", "PeliculaID", copia.PeliculaID);
            return View(copia);
        }

        // POST: Copias/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CopiaID,Deteriorada,formatos,Precio_alquiler,PeliculaID")] Copia copia)
        {
            if (id != copia.CopiaID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(copia);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CopiaExists(copia.CopiaID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["PeliculaID"] = new SelectList(_context.Peliculas, "PeliculaID", "PeliculaID", copia.PeliculaID);
            return View(copia);
        }

        // GET: Copias/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var copia = await _context.Copias
                .Include(c => c.Pelicula)
                .FirstOrDefaultAsync(m => m.CopiaID == id);
            if (copia == null)
            {
                return NotFound();
            }

            return View(copia);
        }

        // POST: Copias/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var copia = await _context.Copias.FindAsync(id);
            _context.Copias.Remove(copia);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CopiaExists(int id)
        {
            return _context.Copias.Any(e => e.CopiaID == id);
        }
    }
}
