﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAppPeliculas.Models
{
    public class Pelicula
    {
        [Key]
        public int PeliculaID { get; set; }
        public string Titulo { get; set; }
        public int Anio { get; set; }

        public string Critica { get; set; }
        public string Caratula { get; set; }
        public ICollection<Copia> Copias { get; set; }

    }
}
