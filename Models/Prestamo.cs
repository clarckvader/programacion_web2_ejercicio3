﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAppPeliculas.Models
{
    public class Prestamo
    {
        [Key]
        public int  PrestamoID { get; set; }

        public DateTime Fecha_Prestamos { get; set; }
        public DateTime Fecha_Tope { get; set; }
        public DateTime fecha_entrega { get; set; }
        public int ClienteID { get; set; }
        public int CopiaID { get; set; }

        public Cliente Cliente { get; set; }
        public Copia Copia { get; set; }


    }
}
