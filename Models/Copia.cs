﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAppPeliculas.Models
{
    public enum formatos
    {
        DVD, VHS , DIGITAL
    }
    public class Copia
    {
        [Key]
        public int CopiaID { get; set; }
        public bool Deteriorada { get; set; }
        public formatos formatos { get; set; }

        public decimal Precio_alquiler { get; set; }

        public int PeliculaID { get; set; }

        public Pelicula Pelicula { get; set; }

        public ICollection<Prestamo> Prestamos { get; set; }

    }
}
