﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAppPeliculas.Models
{
    public class Cliente
    {
        [Key]
        public int ClienteID { get; set; }
        public int Ci { get; set; }
        public string nombre { get; set; }
        public string apellidos { get; set; }
        public string direccion { get; set; }
        public string email { get; set; }
        public ICollection<Prestamo> Prestamos { get; set; }
    }
}
